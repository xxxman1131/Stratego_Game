#include "playfield.h"
#include "gamewindow.h"

#include <QBrush>

PlayField::PlayField() {
}

PlayField::PlayField(int board_size, GameWindow* game_window) {
	this->board_size = board_size;
    this->game_window = game_window;

	items = new QGraphicsRectItem*[board_size * board_size];
	for(int i = 0; i < board_size; i++) {
        for(int j = 0; j < board_size; j++) {
            int index = (i * board_size) + j;
			items[index] = new QGraphicsRectItem();
            items[index]->setRect(j * 50, i * 50, 50, 50);
            addItem(items[index]);
		}
	}
}

PlayField::~PlayField() {

}

void PlayField::mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent) {
	if (!scene_lock && mouseEvent->button() == Qt::LeftButton) {
		QGraphicsItem* item = itemAt(mouseEvent->scenePos(), QTransform());
		QGraphicsRectItem* rect = qgraphicsitem_cast<QGraphicsRectItem*>(item);
        if (rect) {
            int selected_square_index = (((int)(mouseEvent->scenePos().y() / 50)) * board_size) +
                                        ((int)(mouseEvent->scenePos().x() / 50));
			if (!board[selected_square_index]) {
				scene_lock = true;
                game_window->squareSelected(selected_square_index);
			}
		}
	}
}

void PlayField::waitForPlayersMove(bool* board) {
	this->board = board;
	scene_lock = false;
}

void PlayField::paintSquare(int index, QBrush* color) {
	items[index]->setBrush(*color);
	items[index]->update();
}
