#ifndef PLAYER_H
#define PLAYER_H

#include <QObject>

//Player types
#define HUMAN_PLAYER 0
#define MIN_MAX 1
#define ALFA_BETA 2

//Node search heuristics
#define BEST_SCORES_FIRST 0
#define ABOVE_AVERAGE_FIRST 1
#define NATURAL_ORDER 2

//Scoring heuristics
#define GREEDY 0
#define PLAY_SAVE 1
#define BALANCED 2

//Difficulty levels
#define NUT 0
#define EASY 1
#define NORMAL 2
#define ADVANCED 3
#define HARD 4
#define HARDCORE 5

//Values
#define NOT_USED_VALUE -1
#define MIN -1000
#define MAX 1000

class Player : public QObject {
    Q_OBJECT
public:
	Player();
	Player(int type, int difficulty, int node_search_heuristics, int scoring_heuristics, int board_size);
	~Player();

    bool isHuman();

public slots:
    void selectSquare(bool* board, int squares_left);

signals:
	void squareFound(int choosen_square);

private:
	int type;
    int difficulty;
	int node_search_heuristics;
    int scoring_heuristics;
	int board_size;
    int total_squares; // board_size * board_size

    // usefull math variables;
    int total_squares_minus_1;
    int board_size_minus_1;
    int board_size_plus_1;

    int minMax(bool* board, int iterations_left);
    int maximizer(bool* board, int score, int iterations_left);
    int minimizer(bool* board, int score, int iterations_left);

    int alfaBeta(bool* board, int iterations_left);
    int alfaMax(bool* board, int score, int iterations_left, int alfa, int beta);
    int betaMin(bool* board, int score, int iterations_left, int alfa, int beta);

    int countScore(bool* board, int selected_square_index, bool maximizerTurn);
    int getSortedScores(int* scores, int* indexes, bool* board, bool maximizerTurn);

};

#endif // PLAYER_H
