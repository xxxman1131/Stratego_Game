#ifndef WINNERDISCLAIMER_H
#define WINNERDISCLAIMER_H

#include <QDialog>

namespace Ui {
class WinnerDisclaimer;
}

class WinnerDisclaimer : public QDialog
{
    Q_OBJECT

public:
    explicit WinnerDisclaimer(QWidget *parent = 0);
    ~WinnerDisclaimer();
    void setStats(int player1_score,int player1_time,int player2_score,int player2_time);

private slots:
    void on_pushButton_clicked();

private:
    Ui::WinnerDisclaimer *ui;
};

#endif // WINNERDISCLAIMER_H
