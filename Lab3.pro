#-------------------------------------------------
#
# Project created by QtCreator 2018-04-27T11:28:41
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Lab3
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    player.cpp \
    gamewindow.cpp \
    playfield.cpp \
    winnerdisclaimer.cpp

HEADERS  += mainwindow.h \
    player.h \
    gamewindow.h \
    playfield.h \
    winnerdisclaimer.h

FORMS    += mainwindow.ui \
    gamewindow.ui \
    winnerdisclaimer.ui
