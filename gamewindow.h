#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QMainWindow>
#include <QBrush>
#include <QThread>
#include <QTime>

namespace Ui {
	class GameWindow;
}

class PlayField;
class Player;

class GameWindow : public QMainWindow {
	Q_OBJECT

public:
    explicit GameWindow(QWidget *parent = 0);
	~GameWindow();

    void setUpGame(int board_size, Player* player1, Player* player2, bool show_ui);

signals:
    void player1Turn(bool* board, int squares_left);
    void player2Turn(bool* board, int squares_left);

public slots:
    void on_startPushButton_clicked();
    void squareSelected(int selected_square_index);

private:
	Ui::GameWindow* ui;
    QThread* off_scene_thread;

	PlayField* play_field;
	Player* player1;
	Player* player2;
	int player1_score;
	int player2_score;
	QBrush* red;
    QBrush* blue;

    QTime timer;
    int player1_time;
    int player2_time;

	int board_size;
	int total_squares; // board_size * board_size
	bool* board;
	int moves_done;
	bool ui_shown;

	void nextTurn();
};

#endif // GAMEWINDOW_H
