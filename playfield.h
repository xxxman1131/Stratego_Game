#ifndef STRATEGOSCENE_H
#define STRATEGOSCENE_H

#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>

class GameWindow;

class PlayField : public QGraphicsScene {
	Q_OBJECT

public:
	PlayField();
	PlayField(int board_size, GameWindow* game_window);
	~PlayField();

	void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent);
	void waitForPlayersMove(bool* board);
	void paintSquare(int index, QBrush* color);

private:
    GameWindow* game_window;

	QGraphicsRectItem** items;
	bool* board;
	int board_size;
	bool scene_lock;

};

#endif // STRATEGOSCENE_H
