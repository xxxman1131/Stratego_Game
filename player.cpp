#include "player.h"

#include <QDebug>

Player::Player() {
}

Player::Player(int type, int difficulty, int node_search_heuristics, int scoring_heuristics, int board_size) {
    this->type = type;
    this->difficulty = difficulty;
    this->node_search_heuristics = node_search_heuristics;
    this->scoring_heuristics = scoring_heuristics;
    this->board_size = board_size;
    this->total_squares = board_size * board_size;

    // definition of usefull math variables
    total_squares_minus_1 = total_squares - 1;
    board_size_minus_1 = board_size - 1;
    board_size_plus_1 = board_size + 1;
}

Player::~Player() {
}

bool Player::isHuman() {
    return type == HUMAN_PLAYER;
}

void Player::selectSquare(bool* board, int squares_left) {
	int choosen_square = -1;
    int iterations = difficulty + 3;
    if (type == MIN_MAX) {
        choosen_square = minMax(board, (iterations > squares_left) ? squares_left : iterations);
    }
    if (type == ALFA_BETA) {
        choosen_square = alfaBeta(board, (iterations > squares_left) ? squares_left : iterations);
    }
    squareFound(choosen_square);
}

int Player::minMax(bool* board, int iterations_left) {
    int best_score = MIN;
    int best_score_index = 0;

    for (int i = 0; i < total_squares; i++) {
        if (!board[i]) {
            board[i] = true;
            int current_score = minimizer(board, countScore(board, i, true), iterations_left - 1);
            if (best_score < current_score) {
                best_score = current_score;
                best_score_index = i;
            }
            board[i] = false;
        }
    }

    return best_score_index;
}

int Player::minimizer(bool *board, int score, int iterations_left) {
    if (iterations_left == 0) return score;

    int worst_score = MAX;

    for (int i = 0; i < total_squares; i++) {
        if (!board[i]) {
            board[i] = true;
            int current_score = maximizer(board, score + countScore(board, i, false), iterations_left - 1);
            if (worst_score > current_score) {
                worst_score = current_score;
            }
            board[i] = false;
        }
    }

    return worst_score;
}

int Player::maximizer(bool *board, int score, int iterations_left) {
    if (iterations_left == 0) return score;

    int best_score = MIN;

    for (int i = 0; i < total_squares; i++) {
        if (!board[i]) {
            board[i] = true;
            int current_score = minimizer(board, countScore(board, i, true) + score, iterations_left - 1);
            if (best_score < current_score) {
                best_score = current_score;
            }
            board[i] = false;
        }
    }

    return best_score;
}

int Player::alfaBeta(bool *board, int iterations_left) {
    int alfa = MIN;
    int beta = MAX;
    int best_score_index = 0;

    if (node_search_heuristics == NATURAL_ORDER) {
        for (int i = 0; i < total_squares; i++) {
            if (!board[i]) {
                board[i] = true;
                int current_score = betaMin(board, countScore(board, i, true), iterations_left - 1, alfa, beta);
                if (alfa < current_score) {
                    alfa = current_score;
                    best_score_index = i;
                }
                board[i] = false;
            }
        }
	} else {
		int* scores = new int[total_squares];
		int* indexes = new int[total_squares];
		int size = getSortedScores(scores, indexes, board, false);
		for (int i = 0; i < size; i++) {
			if (!board[indexes[i]]) {
				board[indexes[i]] = true;
				int current_score = betaMin(board, scores[i], iterations_left - 1, alfa, beta);
                if (alfa < current_score) {
                    alfa = current_score;
					best_score_index = indexes[i];
                }
				board[indexes[i]] = false;
            }
        }
    }

    return best_score_index;
}

int Player::alfaMax(bool *board, int score, int iterations_left, int alfa, int beta) {
    if (iterations_left == 0) return score;

    if (node_search_heuristics == NATURAL_ORDER) {
        for (int i = 0; i < total_squares; i++) {
            if (!board[i]) {
                board[i] = true;
                int current_score = betaMin(board, score + countScore(board, i, true), iterations_left - 1, alfa, beta);
                if (current_score >= beta) {
                    board[i] = false;
                    return MAX;
                }
                if (alfa < current_score) {
                    alfa = current_score;
                }
                board[i] = false;
            }
        }
	} else {
		int* scores = new int[total_squares];
		int* indexes = new int[total_squares];
		int size = getSortedScores(scores, indexes, board, false);

		for (int i = 0; i < size; i++) {
			if (!board[indexes[i]]) {
				board[indexes[i]] = true;
				int current_score = betaMin(board, score + scores[i], iterations_left - 1, alfa, beta);
                if (current_score >= beta) {
					board[indexes[i]] = false;
                    return MAX;
                }
                if (alfa < current_score) {
                    alfa = current_score;
                }
				board[indexes[i]] = false;
            }
        }
    }

    return alfa;
}

int Player::betaMin(bool *board, int score, int iterations_left, int alfa, int beta) {
    if (iterations_left == 0) return score;

    if (node_search_heuristics == NATURAL_ORDER) {
        for (int i = 0; i < total_squares; i++) {
            if (!board[i]) {
                board[i] = true;
                int current_score = alfaMax(board, score + countScore(board, i, false), iterations_left - 1, alfa, beta);
                if (current_score <= alfa) {
                    board[i] = false;
                    return MIN;
                }
                if (beta > current_score) {
                    beta = current_score;
                }
                board[i] = false;
            }
        }
	} else {
		int* scores = new int[total_squares];
		int* indexes = new int[total_squares];
		int size = getSortedScores(scores, indexes, board, false);

		for (int i = 0; i < size; i++) {
			if (!board[indexes[i]]) {
				board[indexes[i]] = true;
				int current_score = alfaMax(board, score + scores[i], iterations_left - 1, alfa, beta);
                if (current_score <= alfa) {
					board[indexes[i]] = false;
                    return MIN;
                }
                if (beta > current_score) {
                    beta = current_score;
                }
				board[indexes[i]] = false;
            }
        }
    }

    return beta;
}

int Player::countScore(bool* board, int selected_square_index, bool maximizerTurn) {
    int total_score = 0;

    // current maths variables
    int moves_col = selected_square_index % board_size;
    int moves_row = selected_square_index / board_size;

    // check from top to bottom
    int starting_index = moves_col;
    int finishing_index = starting_index + total_squares - board_size;
    int score_gain = 0;
    for (int i = starting_index; i <= finishing_index; i += board_size) {
        if (board[i]) score_gain++;
        else { score_gain = 0; i = total_squares; }
    }
    total_score += score_gain;

    // check from left to right
    starting_index = selected_square_index - moves_col;
    finishing_index = starting_index + board_size_minus_1;
    score_gain = 0;
    for (int i = starting_index; i <= finishing_index; i++) {
        if (board[i]) score_gain++;
        else { score_gain = 0; i = total_squares; }
    }
    total_score += score_gain;

    // check from top left to bottom right
    if (moves_col < moves_row) {
        starting_index = (moves_row - moves_col) * board_size;
        finishing_index = total_squares_minus_1 - moves_row + moves_col;
    } else {
        starting_index = moves_col - moves_row;
        finishing_index = total_squares_minus_1 - (board_size * starting_index);
    }
    score_gain = 0;
    for (int i = starting_index; i <= finishing_index; i += board_size_plus_1) {
        if (board[i]) score_gain++;
        else { score_gain = 0; i = total_squares; }
    }
    total_score += score_gain;

    // check from top right to bottom left
    if (moves_col + moves_row < board_size) {
        starting_index = moves_row + moves_col;
        finishing_index = starting_index * board_size;
    } else {
        starting_index = selected_square_index - (board_size_minus_1 - moves_col) * board_size_minus_1;
        finishing_index = selected_square_index + (board_size_minus_1 - moves_row) * board_size_minus_1;
    }
    score_gain = 0;
    for (int i = starting_index; i <= finishing_index; i += board_size_minus_1) {
        if (board[i]) score_gain++;
        else { score_gain = 0; i = total_squares; }
    }
    total_score += score_gain;

    if (scoring_heuristics == GREEDY) {
        return maximizerTurn ? total_score * 2 : total_score * -1;
    }

    if (scoring_heuristics == PLAY_SAVE) {
        return maximizerTurn ? total_score : total_score * -2;
    }

    if (scoring_heuristics == BALANCED) {
        return maximizerTurn ? total_score : total_score * -1;
    }

    return 0;
}

int Player::getSortedScores(int* scores, int* indexes, bool *board, bool maximizerTurn) {
	int size = 0;

	if (node_search_heuristics == ABOVE_AVERAGE_FIRST) {
        int sum = 0;

		for (int i = 0; i < total_squares; i++) {
            if (!board[i]) {
				board[i] = true;
				scores[size] = countScore(board, i, maximizerTurn);
				indexes[size] = i;
				sum += scores[size];
				size++;
                board[i] = false;
            }
        }

		int average = sum / size;
		int last = size - 1;
		int first = 0;

        if (maximizerTurn) {
            while (first < last) {
				if (scores[first] > average) {
					if (scores[last] <= average) {
						int tmp = scores[first];
						scores[first] = scores[last];
						scores[last] = tmp;
						tmp = indexes[first];
						indexes[first] = indexes[last];
						indexes[last] = tmp;
                        first++;
                        last--;
                    } else {
                        last--;
                    }
                } else {
                    first++;
                }
            }
        } else {
            while (first < last) {
				if (scores[first] <= average) {
					if (scores[last] > average) {
						int tmp = scores[first];
						scores[first] = scores[last];
						scores[last] = tmp;
						tmp = indexes[first];
						indexes[first] = indexes[last];
						indexes[last] = tmp;
                        first++;
                        last--;
                    } else {
                        last--;
                    }
                } else {
                    first++;
                }
            }
        } // ABOVE AVERAGE FIRST

    } else {
        for (int i = 0; i < total_squares; i++) {
            if (!board[i]) {
                board[i] = true;
				scores[size] = countScore(board, i, maximizerTurn);
				indexes[size] = i;
				size++;
                board[i] = false;
            }
        }

        if (maximizerTurn) {
            bool swapp = true;
            while(swapp){
                swapp = false;
				for (int i = 0; i < size - 1; i++) {
					if (scores[i] > scores[i + 1]) {
						int tmp = scores[i];
						scores[i] = scores[i + 1];
						scores[i + 1] = tmp;
						tmp = indexes[i];
						indexes[i] = indexes[i + 1];
						indexes[i + 1] = tmp;
                        swapp = true;
                    }
                }
            }
        } else {
            bool swapp = true;
            while(swapp){
                swapp = false;
				for (int i = 0; i < size - 1; i++) {
					if (scores[i] < scores[i + 1]) {
						int tmp = scores[i];
						scores[i] = scores[i + 1];
						scores[i + 1] = tmp;
						tmp = indexes[i];
						indexes[i] = indexes[i + 1];
						indexes[i + 1] = tmp;
                        swapp = true;
                    }
                }
            }
        }
    } // BEST SCORES FIRST

	return size;
}
