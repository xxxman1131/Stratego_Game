#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <vector>

namespace Ui {
	class MainWindow;
}

class GameWindow;

class MainWindow : public QMainWindow {
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

private slots:
	void on_playPushButton_clicked();
	void on_playWithoutUiPushButton_clicked();
	void on_quitPushButton_clicked();

private:
	Ui::MainWindow* ui;
	std::vector<GameWindow*> active_games;

};

#endif // MAINWINDOW_H
