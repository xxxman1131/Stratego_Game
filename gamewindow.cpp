#include "gamewindow.h"
#include "ui_gamewindow.h"
#include "winnerdisclaimer.h"
#include "playfield.h"
#include "player.h"

GameWindow::GameWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::GameWindow) {
    ui->setupUi(this);
}

GameWindow::~GameWindow() {
    delete player1;
    delete player2;
    if (ui_shown) delete play_field;
    delete[] board;
    off_scene_thread->terminate();
    delete off_scene_thread;
    delete red;
    delete blue;
    delete ui;
}

void GameWindow::setUpGame(int board_size, Player* player1, Player* player2, bool show_ui) {
    this->board_size = board_size;
    total_squares = board_size * board_size;
    moves_done = 0;
    this->ui_shown = show_ui;

    board = new bool[total_squares];
    for (int i = 0; i < total_squares; i++) board[i] = false;

    this->player1 = player1;
    this->player2 = player2;
    player1_score = 0;
    player2_score = 0;
    red = new QBrush(Qt::SolidPattern);
    blue = new QBrush(Qt::SolidPattern);
    red->setColor(Qt::red);
    blue->setColor(Qt::blue);
    player1_time = 0;
    player2_time = 0;

    off_scene_thread = new QThread();
    player1->moveToThread(off_scene_thread);
    player2->moveToThread(off_scene_thread);

    QObject::connect(this, SIGNAL(player1Turn(bool*, int)), player1, SLOT(selectSquare(bool*, int)));
    QObject::connect(this, SIGNAL(player2Turn(bool*, int)), player2, SLOT(selectSquare(bool*, int)));
    QObject::connect(player1, SIGNAL(squareFound(int)), this, SLOT(squareSelected(int)));
    QObject::connect(player2, SIGNAL(squareFound(int)), this, SLOT(squareSelected(int)));

    off_scene_thread->start();

    if (show_ui) {
        this->play_field = new PlayField(board_size, this);
        ui->gameGraphicsView->setScene(play_field);
        ui->gameGraphicsView->update();
    }
}

void GameWindow::on_startPushButton_clicked() {
    nextTurn();
    ui->startPushButton->setVisible(false);
}

void GameWindow::nextTurn() {
    timer.start();
    if (moves_done % 2 == 0) {
        if (player1->isHuman()) {
            play_field->waitForPlayersMove(board);
        } else {
            emit player1Turn(board, total_squares - moves_done);
        }
    } else {
        if (player2->isHuman()) {
            play_field->waitForPlayersMove(board);
        } else {
            emit player2Turn(board, total_squares - moves_done);
        }
    }
}


void GameWindow::squareSelected(int selected_square_index) {
    board[selected_square_index] = true;
    int total_score = 0;

    // maths variables
    int moves_col = selected_square_index % board_size;
    int moves_row = selected_square_index / board_size;
    int total_squares_minus_1 = total_squares - 1;
    int board_size_minus_1 = board_size - 1;
    int board_size_plus_1 = board_size + 1;

    // check from top to bottom
    int starting_index = moves_col;
    int finishing_index = starting_index + total_squares - board_size;
    int score_gain = 0;
    for (int i = starting_index; i <= finishing_index; i += board_size) {
        if (board[i]) score_gain++;
        else { score_gain = 0; i = total_squares; }
    }
    total_score += score_gain;

    // check from left to right
    starting_index = selected_square_index - moves_col;
    finishing_index = starting_index + board_size_minus_1;
    score_gain = 0;
    for (int i = starting_index; i <= finishing_index; i++) {
        if (board[i]) score_gain++;
        else { score_gain = 0; i = total_squares; }
    }
    total_score += score_gain;

    // check from top left to bottom right
    if (moves_col < moves_row) {
        starting_index = (moves_row - moves_col) * board_size;
        finishing_index = total_squares_minus_1 - moves_row + moves_col;
    } else {
        starting_index = moves_col - moves_row;
        finishing_index = total_squares_minus_1 - (board_size * starting_index);
    }
    score_gain = 0;
    for (int i = starting_index; i <= finishing_index; i += board_size_plus_1) {
        if (board[i]) score_gain++;
        else { score_gain = 0; i = total_squares; }
    }
    total_score += score_gain;

    // check from top right to bottom left
    if (moves_col + moves_row < board_size) {
        starting_index = moves_row + moves_col;
        finishing_index = starting_index * board_size;
    } else {
        starting_index = selected_square_index - (board_size_minus_1 - moves_col) * board_size_minus_1;
        finishing_index = selected_square_index + (board_size_minus_1 - moves_row) * board_size_minus_1;
    }
    score_gain = 0;
    for (int i = starting_index; i <= finishing_index; i += board_size_minus_1) {
        if (board[i]) score_gain++;
        else { score_gain = 0; i = total_squares; }
    }
    total_score += score_gain;

    if (moves_done % 2 == 0) {
        player1_time += timer.elapsed();
        player1_score += total_score;
        if (ui_shown) {
            play_field->paintSquare(selected_square_index, red);
            ui->player1PointsLabel->setText(QString::number(player1_score));
            ui->player1TimeLabel->setText(QString::number(player1_time));
        }
    } else {
        player2_time += timer.elapsed();
        player2_score += total_score;
        if (ui_shown) {
            play_field->paintSquare(selected_square_index, blue);
            ui->player2PointsLabel->setText(QString::number(player2_score));
            ui->player2TimeLabel->setText(QString::number(player2_time));
        }
    }

    moves_done++;

    if (moves_done < total_squares) {
        nextTurn();
    } else {
        WinnerDisclaimer* disclaimer = new WinnerDisclaimer();
        disclaimer->setStats(player1_score, player1_time, player2_score, player2_time);
        disclaimer->show();
    }
}
