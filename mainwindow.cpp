#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "gamewindow.h"
#include "player.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow) {
	ui->setupUi(this);
}

MainWindow::~MainWindow() {
	delete ui;
}

void MainWindow::on_playPushButton_clicked() {
	int board_size = ui->boardSizeSpinBox->value();

	Player* player1 = new Player(ui->player1ComboBox->currentIndex(),
								 ui->player1DifficultyComboBox->currentIndex(),
								 ui->player1NodeSearchComboBox->currentIndex(),
								 ui->player1ScorringComboBox->currentIndex(),
								 board_size);

	Player* player2 = new Player(ui->player2ComboBox->currentIndex(),
								 ui->player2DifficultyComboBox->currentIndex(),
								 ui->player2NodeSearchComboBox->currentIndex(),
								 ui->player2ScorringComboBox->currentIndex(),
								 board_size);

    GameWindow* game = new GameWindow();
    game->setUpGame(board_size, player1, player2, true);
    active_games.push_back(game);
    game->show();
}

void MainWindow::on_playWithoutUiPushButton_clicked() {
	int board_size = ui->boardSizeSpinBox->value();

	Player* player1 = new Player(ui->player1ComboBox->currentIndex(),
								 ui->player1DifficultyComboBox->currentIndex(),
								 ui->player1NodeSearchComboBox->currentIndex(),
								 ui->player1ScorringComboBox->currentIndex(),
								 board_size);

	Player* player2 = new Player(ui->player2ComboBox->currentIndex(),
								 ui->player2DifficultyComboBox->currentIndex(),
								 ui->player2NodeSearchComboBox->currentIndex(),
								 ui->player2ScorringComboBox->currentIndex(),
								 board_size);

    GameWindow* game = new GameWindow();
    game->setUpGame(board_size, player1, player2, false);
    if (ui->player1ComboBox->currentIndex() == HUMAN_PLAYER || ui->player2ComboBox->currentIndex() == HUMAN_PLAYER) {
        game->show();
    } else {
        game->on_startPushButton_clicked();
    }
    active_games.push_back(game);
}

void MainWindow::on_quitPushButton_clicked() {
	foreach (GameWindow* game, active_games) {
		delete game;
	}

	close();
}
