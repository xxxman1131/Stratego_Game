#include "winnerdisclaimer.h"
#include "ui_winnerdisclaimer.h"

WinnerDisclaimer::WinnerDisclaimer(QWidget *parent) : QDialog(parent), ui(new Ui::WinnerDisclaimer) {
    ui->setupUi(this);
}

WinnerDisclaimer::~WinnerDisclaimer() {
    delete ui;
}

void WinnerDisclaimer::setStats(int player1_score, int player1_time, int player2_score, int player2_time) {
    ui->player1ScoreLabel->setText(QString::number(player1_score));
    ui->player1TimeLabel->setText(QString::number(player1_time));
    ui->player2ScoreLabel->setText(QString::number(player2_score));
    ui->player2TimeLabel->setText(QString::number(player2_time));
    ui->winnerLabel->setText(QString(player1_score > player2_score ? "player 1 !!!" : "player 2 !!!"));
}

void WinnerDisclaimer::on_pushButton_clicked() {
    close();
}
